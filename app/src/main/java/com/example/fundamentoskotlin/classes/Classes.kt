package com.example.fundamentoskotlin.classes

import com.example.fundamentoskotlin.newTopic

fun main() {
    newTopic("Classes")

    val phone = Phone(121212)
    phone.call()
    phone.showNumber()
    // println(phone.number) is protected not possible to access

    newTopic("Herencia")
    val smartPhone = SmartPhone(1212, true)
    smartPhone.call()

    newTopic("Sobrescritura")
    smartPhone.showNumber()
    println("Privado? ${smartPhone.isPrivate}")

    newTopic("Data Clases")
    val myUser = User(0, "Felipe", "Carrasco", 0)

    println(myUser)
}