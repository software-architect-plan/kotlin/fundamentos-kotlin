package com.example.fundamentoskotlin

const val separator = "================="

fun main() {
    //println("Hello kotlin")
    newTopic("Hola Kotlin")

    newTopic("Variables y constantes")
    // val a = 2
    // a = 4 no se puedo modificar porque es constante
    val a = true
    println(a)

    var b: Int// var b = 2
    b = 3
    // b = "Hola" no coincide el tipo
    println("b = $b")

    var objNull: Any?
    objNull = null
    objNull = "Hi"

    println(objNull)
    showQuantityApples(4)
}

fun newTopic(topic: String) {
    println("$separator $topic $separator")
}

fun showQuantityApples(quantity: Int) {
    println("Tengo $quantity manzanas")
}